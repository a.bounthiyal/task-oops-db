<?php
/**
 * Created by PhpStorm.
 * User: abhishekB
 * Date: 7/27/2018
 * Time: 7:44 PM
 */

class dbConnection
{
  public $server = 'localhost';
  public $user   = 'root';
  public $passwd = '';
  public $db     = 'my_db';
  public $dbCon;
  public $myQuery;

  function __construct(){
    $this->dbCon = mysqli_connect($this->server, $this->user, $this->passwd, $this->db);
  }

}
