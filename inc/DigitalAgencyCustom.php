<?php
/**
 * Created by PhpStorm.
 * User: abhishekB
 * Date: 7/27/2018
 * Time: 4:30 PM
 */

require_once(dirname(__FILE__) . '/DigitalAgency.php');

class DigitalAgencyCustom extends DigitalAgency
{
  protected function validatePerson($person)
  {
    $valName = false;
    $valPosition = false;
    if (is_object($person)) {
      if (property_exists($person, 'name')) {
        if (strlen($person->getName()) > 2) {
          $valName=true;
        }
      }
    }
    if (is_object($person)) {
      if (property_exists($person, 'position')) {
        if (strlen($person->getPosition()) > 4) {
          $valPosition= true;
        }
      }
    }
    return ($valName && $valPosition)? true : false;
  }
}