<?php
/**
 * Created by PhpStorm.
 * User: abhishekB
 * Date: 7/27/2018
 * Time: 3:43 PM
 */

class Person
{
  private $name;
  private $position;

  public function setName($name){
    $this->name=$name;
  }

  public function setPosition($position){
    $this->position=$position;
  }

  public function getName(){
    return $this->name;
  }

  public function getPosition(){
    return $this->position;
  }

  public function getPerson(){
    return [
      'name' => $this->name,
      'position' => $this->position,
    ];
  }
}