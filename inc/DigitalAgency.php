<?php
/**
 * Created by PhpStorm.
 * User: abhishekB
 * Date: 7/27/2018
 * Time: 2:04 PM
 */

require_once(dirname(__FILE__) . '/dbConnection.php');

class DigitalAgency extends dbConnection
{
protected $name;
protected $people;

  public function __construct($name)
  {
    $this->people = [];
    $this->name = $name;

  }

  public function addPerson(Person $person){
    try{
      if ($this->validatePerson($person)){
        array_push($this->people,$person);
      }
      else{
        $error = 'Invalid Parameters';
        throw new Exception($error);
      }
    }
    catch (Exception $e){
      print_r($e);
    }
  }

  public function getPeople(){
   return $this->people;
  }

  public function saveAgency(){

    parent::__construct();
    foreach ($this->people as $people){
      $myQuery = "insert into digitalAgency(id,name,position) values(NULL,'".$people->getName()."','".$people->getPosition()."')";
      $results = mysqli_query($this->dbCon, $myQuery);
      if($results)
      {
        echo "inserted";
      }else
      {
        echo  "not inserted";
      }
    }


  }

  protected function validatePerson($person){
    if(is_object($person)){
      if( property_exists($person, 'name') && property_exists($person, 'position')){
        return true;
      }
    }
    return false;
  }
}