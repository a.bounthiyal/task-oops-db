<?php


require_once (dirname(__FILE__) . '/inc/Person.php');
require_once (dirname(__FILE__) . '/inc/DigitalAgencyCustom.php');

$digitalAgency = new DigitalAgencyCustom('Generation Digital');
$person = new Person();
$person->setName("abhishek");
$person->setPosition("Php Developer");
$digitalAgency->addPerson($person);


$digitalAgency = new DigitalAgencyCustom('Generation Digital');
$person->setName("Vivek");
$person->setPosition("Drupal Developer");
$digitalAgency->addPerson($person);

$digitalAgency->saveAgency();

$people = $digitalAgency->getPeople();

include(dirname(__FILE__).'/templates/masterTemplate.php');